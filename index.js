// import { AppRegistry } from 'react-native';
// import App from './App';

// AppRegistry.registerComponent('albums', () => App);

// Import a library to help create a component
import React from 'react';
import { AppRegistry, View } from 'react-native';
import Header from './src/components/Header';
import AlbumList from './src/components/AlbumList';

// Create a component
const App = () => (
  // Note that the flex: 1 here is required or we get a bug
  // with the scrollview in the child components.
  <View style={{ flex: 1 }}>
    <Header headerText="Albums!" />
    <AlbumList />
  </View>
  
);

// Render it to the device
AppRegistry.registerComponent('albums', () => App);
